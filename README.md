Task details in ```_tz/```  
Task description placed in ```./_tz/PHP_Developer_Test.docx```

Install

```composer insatll```

```npm install && npm run dev```

run migrations

```php artisan migrate```

run to seed data equals CSV (9 objects)

```php artisan db:seed --class=HouseSeederCSV```

OR

run to seed data with random houses (18 objects)

```php artisan db:seed --class=HouseSeeder```
